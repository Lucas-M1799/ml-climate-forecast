import pandas as pd
import numpy as np
import sklearn.model_selection 
from sklearn import metrics
from sklearn.ensemble import RandomForestRegressor

#SOME VARIABLES

id_model = 0 #ID of the model we want to train the algorithm with

if __name__ == '__main__':
	print("\n \n")
	print("** --------------- BEGIN --------------- **")
	print("\n \n")

	print("** --------------- IMPORTING DATA --------------- **")
	print("\n ")
	dataset = pd.read_csv("Data/train_X.csv")

	print("Here are the first lines of the dataset :")
	print(dataset.head())

	print("\n")
	print("Here is the shape of the data :")
	nline, ncol=dataset.shape
	print("SHAPE = ", nline, ncol)

	print("\n \n")
	print("** --------------- PREPARING DATA FOR TRAINING --------------- **")
	print("\n ")
	
	iddata = np.array(dataset['DATASET'])
	idmodel = np.array(dataset['MODEL'])
	idtime = np.array(dataset['TIME'])
	idpos = np.array(dataset['POSITION'])
	value = np.array(dataset['VALUE'])
	
	X_train = np.zeros((5,10*3072))
	#look for the 10 first years of the 22 models
	for k in range(5):
		idx = np.where((iddata==k)*(idmodel==id_model)*(idtime<10))[0]
		if len(idx)<1000:
			print('id data(%d) or id model (%d) unknown',id_data,id_model)
			raise 'IdError'
		model=np.zeros([10*3072])
		model[3072*idtime[idx]+ idpos[idx]]=value[idx]
		X_train[k] = model
		
	Y_train = np.zeros((5,192))
	for k in range(5):
		#look for the year 10 of the 22 models (prediction)
		idx = np.where((iddata==k)*(idmodel==id_model)*(idtime==10))[0]
		pred_model=np.zeros([192])
		pred_model[idpos[idx]]=value[idx]
		Y_train[k] = pred_model
		#print(model, model.shape) #DEBUG
		#print(pred_model, pred_model.shape) #DEBUG
		
	#print(X_train)#Debug
	#print(Y_train)#Debug
		
	
	print("\n \n")
	print("** --------------- TRAINING THE RANDOM FOREST --------------- **")
	print("\n ")
	
	regressor = RandomForestRegressor(n_estimators=50, random_state=0) #Not sure if 50 is a lot or not, seems a lot :/
	#Training phase
	regressor.fit(X_train, Y_train)
	
	
	
	print("\n \n")
	print("** --------------- TESTING THE REGRESSION MODEL --------------- **")
	print("\n ")
	print("       IMPORTING TEST FILE \n")
	datatest_X = pd.read_csv("Data/test_X.csv")
	print(datatest_X.head(), "\n")
	
	datatest_Y = pd.read_csv("Data/test_Y.csv")
	print(datatest_Y.head(), "\n")
	
	print("       PREPARING THE TEST DATA \n")
	
	X_test = np.zeros((2,10*3072))
	#look for the 10 first years of the 22 models
	
	iddata = np.array(datatest_X['DATASET'])
	idmodel = np.array(datatest_X['MODEL'])
	idtime = np.array(datatest_X['TIME'])
	idpos = np.array(datatest_X['POSITION'])
	value = np.array(datatest_X['VALUE'])
	
	for k in range(2):
		idx = np.where((iddata==0)*(idmodel==id_model)*(idtime<10))[0]
		if len(idx)<1000:
			print('id data(%d) or id model (%d) unknown',id_data,id_model)
			raise 'IdError'
		model = np.zeros(10*3072)
		model[3072*idtime[idx]+ idpos[idx]]=value[idx]
		X_test[k] = model
		
	Y_test = np.zeros((2,192))
	for k in range(2):
		#look for the year 10 of the 22 models (prediction)
		idx = np.where((iddata==k)*(idmodel==id_model)*(idtime==10))[0]
		pred_model=np.zeros([192])
		pred_model[idpos[idx]]=value[idx]
		Y_train[k] = pred_model
	
	print("       COMPUTING PREDICTION FOR THE DATA \n")
	
	Y_pred_test = np.zeros((2,192))
	
	Y_pred_test[0] = regressor.predict(X_test[0].reshape(1,-1))
	Y_pred_test[1] = regressor.predict(X_test[1].reshape(1,-1))
	
	print("       RESULTS \n")
	
	print("Here is the prediction for the first set :", Y_pred_test[0], "\n")
	print("Here is the observed results :", Y_test[0], "\n")
	print("Here is the prediction for the second set :", Y_pred_test[1], "\n")
	print("Here is the observed results :", Y_test[1], "\n")
	
	
